from django.db import models
import datetime

class Article(models.Model):
	article_name = models.CharField(max_length=200, null=True, default='')

	article_occurences = models.IntegerField(null=True, default=1)
	def update_article_count(self):
		self.article_occurences += 1
		self.save()

	def time_under_timezone():
		return datetime.datetime.now(datetime.timezone.utc)
	creation_date = models.DateTimeField('Date of Creation', default=time_under_timezone)

	def __str__(self):
		return self.article_name