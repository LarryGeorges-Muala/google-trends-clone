from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.conf import settings
from django.template.loader import get_template, render_to_string
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.template import Context
from website.models import Article
import requests
import csv

def generate_csv_for_db_sample_data():
	try:
		with open('integration_data_sample.csv', 'w') as csvfile:
			fieldnames = ['Date Created', 'Article Name', 'Number of searches']
			writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
			writer.writeheader()
			for data in Article.objects.all():
				extracted_values = {
					'Date Created': data.creation_date,
					'Article Name': data.article_name,
					'Number of searches': data.article_occurences,
				}
				writer.writerow(extracted_values)
			return True
	except Exception as error:
		print(error)
	return False

def index(request):
	research_item = request.GET.get('search-item', '')
	if research_item:
		try:
			word_processed = Article.objects.get(article_name=research_item.lower())
			word_processed.update_article_count()
		except MultipleObjectsReturned:
			word_processed = Article.objects.filter(article_name=research_item.lower()).order_by('id').first()
			word_processed.update_article_count()
		except ObjectDoesNotExist:
			Article.objects.create(
				article_name = research_item.lower()
			)
		except Exception as error:
			print(error)

	context = {'research_item': research_item}
	return render(request, 'website/index.html', context)
